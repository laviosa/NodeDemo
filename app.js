var express = require ('express');
var passport = require('passport');
var app = express();
var port = process.env.PORT || 3000;
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require ("mongoose");
var config = require('./config/database');
var User = require('./app/models/user');
var Note = require('./app/models/note');
var jwt = require('jwt-simple');
var uniqid = require('uniqid');
var generateSafeId = require('generate-safe-id');

//body-parser
app.use(bodyParser.urlencoded({
	extended:true
}));
app.use(bodyParser.json());

//log to console
app.use(morgan('dev'));

//use the passport package 
app.use(passport.initialize());

//demo Route(GET http://localhost:3000)
app.get("/",(req,res)=>{
	res.send('The API is at http://localhost:'+port);
});

//connect to database
mongoose.connect(config.database);

//pass passport for configuration 
require('./config/passport')(passport);

//bundle our routes
var apiRoutes = express.Router();

//create a new user account (post http://localhost:3000/user/signup)
apiRoutes.post('/user/signup',function(req,res){
	console.log(req.body);
	if (req.body.email == null || req.body.password == null || req.body.username == null) {
		console.log(req.body.email);	
		res.json({success: false, msg: 'Please pass username , email and password.'});
	}
	else {
		User.findOne({
			email: req.body.email
		}, function(err, user) {
			if (err) throw err;
			if (!user) {
				var new_id =  generateSafeId();	
				var newUser = new User({
					user_id:new_id,
					email:req.body.email,	
					username: req.body.username,
					password: req.body.password,

				});
    				// save the user
    				newUser.save(function(err,user) {
    					if (err) {
    						return res.json({code:401, success: false, msg: 'User already exists.'});
    					}  	    
    					user.token = 'JWT '+ jwt.encode(user, config.secret);
    					user.save(function(err,user1){
    						res.json({ code: 200, success:true, msg: 'Successfully created new user',data: {token:user1.token,user_id:user1.user_id,username:user1.username,email:user1.email}});	
    					});
    				});
    			}

    			else{
    				res.send({ code :401, success: false, msg: 'Authentication failed.'});
    			}
    		});
	}
});

// route to authenticate and login a user (POST http://localhost:8080/user/login)
apiRoutes.post('/user/login', function(req, res) {
	User.findOne({email: req.body.email}, function(err, user) {
		if (err) throw err;

		if (!user) {
			res.send({ code:401, success: false, msg: 'User not found. Please sign up at http://localhost:3000/user/signup'});
		}

		else {		
      				// check if password matches
      				user.comparePassword(req.body.password, function (err, isMatch) {
      					if (isMatch && !err) {

         					//update new token
         					var new_id = 'JWT '+ jwt.encode(user, config.secret);
         					var myquery1 = { email:req.body.email };
         					var newvalues = { $set: { token: new_id } };
         					User.updateOne(myquery1, newvalues, function(err, res) {
         						if (err) throw err;
         						console.log("1 document updated");
         					});
         					// return the information including token as JSON
         					res.json({ code:200, success: true, data:{user_id:user.user_id , username:user.username, email:user.email, token:new_id }});

         				} 
         				else {
         					res.send({code:401, success: false, msg: 'Authentication failed. Wrong password.'});
         				}
         			});
      			}		
      		});
});



//logout api
apiRoutes.post('/user/logout', function(req, res) {
	var token = getToken(req.headers);
	if (token) {
		var decoded = jwt.decode(token, config.secret);

		User.findOne({email: decoded.email}, function(err, user) {
			if (err) throw err;
			
			if (user) {
				var new_id = undefined;
				var myquery1 = { email:decoded.email };
				var newvalues = { $set: { token: new_id } };
				User.updateOne(myquery1, newvalues, function(err, res) {
					if (err) throw err;
					console.log("1 document updated");
					
				});
				
				res.json({ code:200, success: true,msg:'successfully loggedout'});
				
			}
		});
	}		
	else {
		return res.status(403).send({ success: false, msg: 'No token provided.' });
	}


});


//function tto allow request from different domains
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
	next();
});


//adds a user created note to database
apiRoutes.post('/note/add-new-note',passport.authenticate('jwt', { session: false }),function(req,res){
	console.log(req.headers);
	var token = getToken(req.headers);
	if (token) {
		var decoded = jwt.decode(token, config.secret);

		User.findOne({email: decoded.email}, function(err, user) {
			if (err) throw err;

			if (user) {
				var new_note_id =  uniqid();	
				var newNote = new Note({
					note_id:new_note_id,
					user_id:req.body.user_id,
					token:req.headers.token,	
					note_title: req.body.note_title,
					note_discription: req.body.note_description,

				});
    		// save the note
    		newNote.save(function(err,note) {
    			if (err) {
    				console.log(err);
    				return res.json({code:401, success: false, msg: 'Error occured.'});
    			}  	    
    			res.json({ code: 200, success:true, msg: 'Successfully created new note' });	
    		});  	
    	}


    	else{
    		res.send({ code :401, success: false, msg: 'Access denied.signup to continue'});
    	}
    });
	}

	else {
		return res.status(403).send({ success: false, msg: 'No token provided.' });
	}
});


//function to generate token
getToken = function (headers) {
	if (headers && headers.authorization) {
		var parted = headers.authorization.split(' ');
		if (parted.length === 2) {
			return parted[1];
		} 
		else {
			return null;
		}
	} 
	else {
		return null;
	}
};


//delete a note
apiRoutes.delete('/note/delete_note',passport.authenticate('jwt', { session: false }),function(req,res){
	console.log(req.headers);
	var token = getToken(req.headers);

	if (token) {
		var decoded = jwt.decode(token, config.secret);

		User.findOne({email: decoded.email}, function(err, user) {
			if (err) throw err;

			if (user) {
				
				//delete the note
				var myquery = {note_id: req.query.note_id};
				
				Note.findOne(myquery,function(err,note){
					if(err) throw err;
					
					else if(note){
						Note.deleteOne(myquery, function(err, results) {
							if (err)
								res.send(err);
							else if(results){
								console.log(results.deletedCount);
								res.json({code:200,success:true, message: 'note successfully deleted' });
							}
							else
								res.json({code:400,success:false,msg:'some Error occured'});

						});
					}

					else
						res.json({code:200,success:true,msg:'no such note found'});
				})
				

			}	
			else {
				res.send({ code :401, success: false, msg: 'Access denied.'});
			}
		});
	}

	else {
		return res.status(403).send({code:403 ,success: false, msg: 'No token provided.' });
	}
});


//reterive all notes of user
apiRoutes.get('/note/send-all-notes',passport.authenticate('jwt', { session: false }),function(req,res){
	var token = getToken(req.headers);
	if (token) {
		var decoded = jwt.decode(token, config.secret);

		User.find({email: decoded.email}, function(err, user) {
			if (err) throw err;

			if (user) {

    			//reterive the note
    			Note.find({user_id:req.query.user_id},function(err,note){
    				if(err)
    					res.send(err);
    				else if(note)
    					res.json({code:200,success:true,msg:'Successfully reterived', data:{note}});
    				else
    					res.json({code:200,success:true,msg:'no such note found'});
    			});
    			
    		}	
    		else{
    			res.send({ code :401, success: false, msg: 'Access denied.'});
    		}
    	});
	}

	else {
		return res.status(403).send({code:403, success: false, msg: 'No token provided.' });
	}
});

//reterive a note
apiRoutes.get('/note/send-note',passport.authenticate('jwt', { session: false }),function(req,res){
	var token = getToken(req.headers);
	if (token) {
		var decoded = jwt.decode(token, config.secret);

		User.find({email: decoded.email}, function(err, user) {
			if (err) throw err;

			if (user) {
				//reterive the note
				
				var myquery = {note_id:req.query.note_id};
				Note.findOne(myquery, function(err,note) {
					if(err)
						res.json(err);
					else if(note)
						res.json({code:200,success:true,msg:'Successfully reterived', data:{note_id:note.note_id,note_title:note.note_title,note_discription:note.note_discription}});
					else
						res.json({code:200,success:true,msg:'no such note found'});
				});
			}	
			else{
				res.send({ code :401, success: false, msg: 'Access denied.'});
			}
		});
	}

	else {
		return res.status(403).send({code:403, success: false, msg: 'No token provided.' });
	}
});


// connect the user routes under /user/*
app.use('', apiRoutes);

//start the server

app.listen(port,()=>{
	console.log("Server listening on port" + port);
});





