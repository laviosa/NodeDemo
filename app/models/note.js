var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NoteSchema = new Schema({
	note_id: {
		type : String,
		unique:true,
		required: true
	},
	user_id: {
		type: String,
		required: true
	},
	note_title: {
		type: String,
		required:true
	},
	note_discription:{
		type: String,
		required:true
	}

});

module.exports = mongoose.model('Note',NoteSchema);